function generate_data()

    % points_per_cluster = 10000;
    points_per_cluster = 1000;
    vec_dim = 2;
    k_clusters = 16;
    
    PT = zeros(points_per_cluster * k_clusters, 2);
    
    figure; hold on;
    for k=1:k_clusters
    	mu = unifrnd(1, 8, 1, vec_dim);
        sigma = unifrnd(0.4, 0.7) * eye(vec_dim);
        sigma(1, 1) = unifrnd(0.01, 0.5) * sigma(1, 1);
        sigma(2, 2) = unifrnd(0.01, 0.5) * sigma(2, 2);
        R = mvnrnd(mu, sigma, points_per_cluster);
        st_index = (k - 1) * points_per_cluster + 1;
        PT(st_index:st_index + points_per_cluster-1, :) = R;
        plot(R(:, 1), R(:, 2), '.');
    end
    ordering = randperm(size(PT, 1));
    PT_SH = PT(ordering, :);
    
    grid on;
    xlabel('X');
    ylabel('Y');
    
    % fp = fopen('exp_16_2.txt', 'w');
    % for i=1:size(PT_SH, 1)
    %     fprintf(fp, '%.4f %.4f\n', PT_SH(i, :));
    % end
    % fclose(fp);

end

