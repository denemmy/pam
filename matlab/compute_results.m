function read_data()

    file_path = fullfile('..', 'data', 'data_res.txt');
    fp = fopen(file_path, 'r');    
    format = '%d,%d,%d,%d,%d,%d,%d,%d';    
    C = textscan(fp, format);
    fclose(fp);
    
    n_proc_sample = 29;
    
    n_size = double(C{1});
    m_dim = double(C{2});
    k_clus = double(C{3});
    p_proc = double(C{4});
    build_time = double(C{5});
    swap_time = double(C{6});
    all_time = double(C{7});
    iter_num = double(C{8});
    
    n_size = reshape(n_size, n_proc_sample, []);
    p_proc = reshape(p_proc, n_proc_sample, []);
    k_clus = reshape(k_clus, n_proc_sample, []);
    iter_num = reshape(iter_num, n_proc_sample, []);
    all_time = reshape(all_time, n_proc_sample, []);
    
    gflops = compute_GFlops(n_size, k_clus, iter_num, all_time);
    efficiency = compute_efficiency(p_proc, iter_num, all_time);
    
    X = n_size;
    Y = p_proc;
    Z = gflops;
    
    surf(X,Y,Z);
    axis tight
    xlabel('matrix size')
    ylabel('number of processes')
    zlabel('GFlops')    
    
    X = n_size;
    Y = p_proc;
    Z = efficiency;
    
    surf(X,Y,Z);
    axis tight
    xlabel('matrix size')
    ylabel('number of processes')
    zlabel('Efficiency')

end

function efficiency = compute_efficiency(p, it, all_time)

    % last column p == 1
    mask_1 = p(:, end) == 1;
    mask_non1 = p(:, 1:end-1) > 1;
    assert(all(mask_1(:)) && all(mask_non1(:)));
    
    it_gt = it(:, end);
    gt_time = all_time(:, end);
    gt_r = gt_time ./ it_gt;
    
    gt_r = repmat(gt_r, [1, size(p, 2)]);
    
    efficiency = 100 * gt_r ./ (all_time ./ it .* p);

end

function gflops = compute_GFlops(n, k, iter_num, all_time)

    a = (n .* n .* (2 .* k - 1) - k .* k .* (n - 1.0 ./ 2) + n - (3.0 ./ 2) .* k);
    b = iter_num .* (k .* n .* n - 3 .* n .* k + k .* k + (n - 1) .* (n - k) .* k);

    gflops = (a + b) ./ all_time ./ 1e9;

end

