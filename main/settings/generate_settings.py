#! /usr/bin/python

a = [50, 100, 150, 200, 500, 1000, 1500, 2000, 2500]
commands = []
for i in a:
    commands.append(str(i) + " 1 0")
with open("1proc.txt", "w") as f:
    f.write("\n".join(commands))


def Generate(fin, a, b):
    b.reverse()
    commands = []
    for j in b:
        for i in a:
            commands.append(str(i) + " " + str(j) + " 0")

    with open(fin, "w") as f:
        f.write("\n".join(commands))


Generate(
    "256-small.txt",
    [50, 100, 150, 200],
    [16, 32, 64, 128, 256])

Generate(
    "256-medium.txt",
    [500, 1000, 1500, 2000, 2500],
    [16, 32, 64, 128, 256])


Generate(
    "500-small.txt",
    [50, 100, 150, 200],
    [400, 500])

Generate(
    "500-medium.txt",
    [500, 1000, 1500, 2000, 2500],
    [400, 500])


Generate(
    "1000-small.txt",
    [50, 100, 150, 200],
    [600, 800, 1000])

Generate(
    "1000-medium.txt",
    [500, 1000, 1500, 2000, 2500],
    [600, 800, 1000])
